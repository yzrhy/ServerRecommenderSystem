<%-- 
    Document   : foto2
    Created on : Jul 13, 2016, 3:49:10 PM
    Author     : yizrahya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table width="100%" border="1">
        <c:forEach var="id" items="${ids}">
        <tr>
            
            <td>
               <img src="${pageContext.servletContext.contextPath }/ShowFoto?id=${id}" />
            </td>
        </tr>
    </c:forEach>
</table>
    </body>
</html>
