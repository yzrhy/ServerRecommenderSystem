/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.dbo;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.me.model.*;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;

/**
 *
 * @author yizrahya
 */
public class TourDBO {
    
//    public HashMap<String, Integer> cache = new HashMap<String, Integer>();
    Connection con;
    public static final String[] PARENTS = new String[] {"wisata bahari","wisata alam","eko wisata","wisata buatan", "wisata sejarah & budaya", "wisata religi", "wisata belanja", "wisata kuliner", "agrowisata"};
    public ArrayList<String> GetSitesName(String keyword,int idWilayah, int budget){
        ArrayList<String> sitesName = new ArrayList();
        
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
//        String qryWilayah = "and idWilayah = ? ";
//        String qrybudget = "and budget between 0 and ?"; 
        try {
            String qry = "SELECT nama FROM tempatwisata WHERE keyword like ?";
            String qryWilayah = "and wilayah = ? ";
            String qrybudget = "and HTM between 0 and ? ";
            
            if(idWilayah != 0){
                qry+= qryWilayah; 
            }
            
//            qry += qrybudget;
            
            PreparedStatement pstm = con.prepareStatement(qry);
            pstm.setString(1, '%'+keyword+'%');
             if(idWilayah != 0){
//                qry+= qryWilayah; 
                  pstm.setInt(2, idWilayah);
            }
           
//            pstm.setInt(3, budget);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                sitesName.add(result.getString("nama"));
            }
            return sitesName;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mycon.logOff();
        }
        
        return sitesName;
    }
    
    public ArrayList<String> GetSiteNameWAnd(ArrayList<String> keyword,int idWilayah, int budget){
         ArrayList<String> sitesName = new ArrayList();
        
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
//        String qryWilayah = "and idWilayah = ? ";
//        String qrybudget = "and budget between 0 and ?"; 
        String query = "select nama from tempatwisata where keyword like";
        String qryAnd = "and keyword like ";
//        String qryAnd = "and";
        String qryOr = "or keyword like ";
//        String reg = "and idWilayah like ?";
        
       query +=  " '%" +keyword.get(0) +"%' ";
        
        for (int i = 1; i < keyword.size(); i++) {
            query += qryAnd + " '%" +keyword.get(i) +"%' ";
            System.out.println("qry : "+query);
        }
        
        if(idWilayah != 0){
         query += "and wilayah = '" + idWilayah + "'";    
        }
        
        System.out.println("final qry ==== " + query);
        
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                sitesName.add(result.getString("nama"));
            }
            return sitesName;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mycon.logOff();
        }
        
        return sitesName;
    }
    
     public ArrayList<String> GetSiteNameWOR(ArrayList<String> keyword,int idWilayah, int budget){
         ArrayList<String> sitesName = new ArrayList();
        
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
//        String qryWilayah = "and idWilayah = ? ";
//        String qrybudget = "and budget between 0 and ?"; 
        String query = "select nama from tempatwisata where keyword like";
        String qryAnd = "and keyword like ";
//        String qryAnd = "and";
        String qryOr = "or keyword like ";
        
       query +=  " '%" +keyword.get(0) +"%' ";
        
        for (int i = 1; i < keyword.size(); i++) {
            query += qryOr + " '%" +keyword.get(i) +"%' ";
            System.out.println("qry : "+query);
        }
        
//        if(idWilayah != 0){
//         query += "and wilayah = '" + idWilayah + "'";    
//        }
        
        System.out.println("final qry ==== " + query);
        
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                sitesName.add(result.getString("nama"));
            }
            return sitesName;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mycon.logOff();
        }
        
        return sitesName;
    }
    
    public ArrayList<String> GetSitesName2(ArrayList<String> keyword,int idWilayah, int budget){
        ArrayList<String> sitesName = new ArrayList();
        
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
//        String qryWilayah = "and idWilayah = ? ";
//        String qrybudget = "and budget between 0 and ?"; 
        String query = "select nama from tempatwisata where keyword like";
        String qryAnd = "and keyword like ";
//        String qryAnd = "and";
        String qryOr = "or keyword like ";
        
       query +=  " '%" +keyword.get(0) +"%' ";
        
        for (int i = 1; i < keyword.size(); i++) {
             boolean isParent = Arrays.asList(PARENTS).contains(keyword.get(i));
             if(!isParent){
                query += qryAnd + " '%" +keyword.get(i) +"%' ";
            }else{
                query += qryOr + " '%" +keyword.get(i) +"%' ";
            }
            System.out.println("qry : "+query);
        }
        
        System.out.println("final qry ==== " + query);
        
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                sitesName.add(result.getString("nama"));
            }
            return sitesName;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mycon.logOff();
        }
        
        return sitesName;
    }
    
    
    
    public ArrayList<TourSite> TopSites(ArrayList<String> sites, int idWilayah, int budget){
        ArrayList<String> topSitesName = new ArrayList();
        ArrayList<TourSite> topSites = new ArrayList();
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        System.out.println("sz" + sites.size());
        PreparedStatement pstm = null;
        try {
            String qry = "Select * From tempatwisata where nama like ?";
//            String
            String qryOrder = " order by poin asc limit 5";
            String queryName = "";
            String qryWilayah = "and wilayah = ? ";
            String qrybudget = "and HTM between 0 and ? "; 
            System.out.println("SIZEEEE"+sites.size());
            if(sites.size() == 1)
            {
                pstm = con.prepareStatement(qry);
                pstm.setString(1, '%'+sites.get(0)+'%');
            }
            else
            {
                for (int i = 1; i < sites.size(); i++) {
                    System.out.println("QRYY 0000" + qry);
                       qry += " or nama like ? ";
                       System.out.println("QRYY 11111" + qry);
                }
                if(idWilayah != 0){
                qry += qryWilayah;
                }
                if(budget != 0){
                qry += qrybudget;
                }
                
                qry += qryOrder;
                pstm = con.prepareStatement(qry);
                pstm.setString(1, '%'+ sites.get(0) + '%');
                int qryIndex = sites.size();
                for (int i = 1; i < sites.size(); i++) {
                    pstm.setString(i+1,'%'+ sites.get(i) + '%');
//                    qryIndex = i;
                }
                
                System.out.println("wilayah" + idWilayah +"==== bdget " +budget);
                if(idWilayah != 0){
                    System.out.println("qryidx "+qryIndex);
                    qryIndex++;
                    System.out.println("qryidx "+qryIndex);
                    pstm.setInt(qryIndex, idWilayah);
                }
                if(budget != 0){
                    System.out.println("qryidx "+qryIndex);
                    qryIndex++;
                    System.out.println("qryidx "+qryIndex);
                    pstm.setInt(qryIndex, budget);
                }
                
                
            }
            
            System.out.println("QRYY " + qry);
            ResultSet results = pstm.executeQuery();
            while(results.next())
            {
                int tourSiteid = Integer.parseInt(results.getString("idTempatWisata"));
                String siteName = results.getString("nama");
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" + siteName);
                String siteDesc = results.getString("deskripsi");
                int siteHTM = Integer.parseInt(results.getString("HTM"));
                String siteFoto = results.getString("foto");
                String siteWilayah = results.getString("wilayah");
                String siteKeyword = results.getString("keyword");
                int sitePoint = Integer.parseInt(results.getString("poin"));
                TourSite toursite = new TourSite(tourSiteid, siteName, siteDesc, siteHTM, siteFoto, siteWilayah, siteKeyword, sitePoint);
                topSites.add(toursite);
                
                System.out.println("ADDDD"+results.getString("nama") + "=====" + results.getString("poin"));
//                topSitesName.add(results.getString("nama"));
            }
            
            return topSites;
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mycon.logOff();
        }
        
        return topSites;
    }
    
    
    public TourSite GetSiteDetails(String sitename)
    {
        TourSite tourSite = null;
        MyConnection myCon = new MyConnection();
        String query = "SELECT * FROM tempatwisata WHERE nama like ?";
        con = myCon.getConnection();
        System.out.println("BBBBBBBB"+sitename);
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1, '%'+sitename+'%');
            ResultSet result = pstm.executeQuery();
            if(result.next())
            {
                int tourSiteid = Integer.parseInt(result.getString("idTempatWisata"));
                String siteName = result.getString("nama");
                
                String siteDesc = result.getString("deskripsi");
                int siteHTM = Integer.parseInt(result.getString("HTM"));
                String siteFoto = result.getString("foto");
                String siteWilayah = result.getString("wilayah");
                String siteKeyword = result.getString("keyword");
                int sitePoint = Integer.parseInt(result.getString("poin"));
                tourSite = new TourSite(tourSiteid, siteName, siteDesc, siteHTM, siteFoto, siteWilayah, siteKeyword, sitePoint);
                System.out.println("asdasdasddasdasd"+tourSite.getNama());
                return tourSite;
            }
            
           
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            myCon.logOff();
        }
        
        return tourSite;
        
        
    }
    
    public void insertKeywordHistory(String siteName){
        MyConnection myCon = new MyConnection();
        System.out.println("SITENAMENYA ADALAH"+siteName);
        con = myCon.getConnection();
        
        String query = "INSERT INTO keywordhistory VALUES (NOW(), ?, 1) ON DUPLICATE KEY UPDATE times = times +1";
        
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1, siteName);
            
            int result = pstm.executeUpdate();
            if(result != 0)
            {
                System.out.println("berhasil update keyword");
            }
            else
            {
                System.out.println("gagal update keyword");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
           myCon.logOff();
        }
    }
    
    
    public void updateSitePoint(String siteName)
    {
//        UPDATE tempatwisata SET poin = poin + 1 WHERE nama LIKE '%bukit kasih%' 
        MyConnection myCon = new MyConnection();
        System.out.println("SITENAMENYA ADALAH"+siteName);
        con = myCon.getConnection();
        String query = "UPDATE tempatwisata SET poin = poin + 1 WHERE nama LIKE ?";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1,  '%'+siteName+'%');
            int result = pstm.executeUpdate();
            if(result == 1)
            {
                System.out.println("berhasil update");
            }
            else
            {
                System.out.println("gagal update");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            myCon.logOff();
        }
        
    }
    
    public void insertTopRecomendations(TourSite tourSite){
        
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "INSERT INTO TopRecomendation (id, nama, poin) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE  poin = poin+1";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, tourSite.getId());
            pstm.setString(2, tourSite.getNama());
            pstm.setInt(3, 1);
            int rs = pstm.executeUpdate();
            if(rs == 1){
                System.out.println("berhasil update");
            }
            else{
                System.out.println("gagal update");
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
           mycon.logOff();
        }
        
    }
    
    
    public void InsertHistory(String siteName){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Insert into recommendationhistory values(NOW(),?, 1) ON DUPLICATE KEY UPDATE times = times +1";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
//            String date  = "2016-12-25";
//            Date myDate = formatter.parse(date.toString());
//            java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
//            java.sql.Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());
//            pstm.setTimestamp(1, sqlDate);
            pstm.setString(1, siteName);
            int rs = pstm.executeUpdate();
              if(rs == 1){
                System.out.println("berhasil update");
            }
            else{
                System.out.println("gagal update");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
           mycon.logOff();
        }
        
        
    }

    public ArrayList<TourSite> GetTopRecommendations(){
        ArrayList<TourSite> toursites = new ArrayList<>();
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        
        String query = "SELECT a.*, b.poin AS 'poinRecommended' " +
                       "FROM tempatwisata a, toprecomendation b " +
                       "WHERE a.nama = b.nama ORDER BY b.poin DESC LIMIT 5";
        
        try {
            PreparedStatement pstm =  con.prepareStatement(query);
            ResultSet result = pstm.executeQuery();
            while(result.next()) {
                TourSite tourSite = new TourSite(); 
                int tourSiteid = Integer.parseInt(result.getString("idTempatWisata"));
                String siteName = result.getString("nama");
                String siteDesc = result.getString("deskripsi");
                int siteHTM = Integer.parseInt(result.getString("HTM"));
                String siteFoto = result.getString("foto");
                String siteWilayah = result.getString("wilayah");
                String siteKeyword = result.getString("keyword");
                int sitePoint = Integer.parseInt(result.getString("poin"));
                
                tourSite = new TourSite(tourSiteid, siteName, siteDesc, siteHTM, siteFoto, siteWilayah, siteKeyword, sitePoint);
                toursites.add(tourSite);
            }
            return toursites;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return toursites;
        
    }
    
    public ArrayList<HistoryView> GetHistory(String dateStart, String dateEnd){
        ArrayList<HistoryView> histories = new ArrayList<>();
//        String query =  "SELECT b.dates,a.nama, b.times " +
//                        "FROM tempatwisata a , recommendationhistory b " +
//                        " WHERE a.nama = b.nama " +
//                        " AND dates BETWEEN ? AND ?";
        String query = "select * from recommendationhistory where dates BETWEEN ? AND ? order by dates asc";
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1,  dateStart);
            pstm.setString(2,  dateEnd);
            
            ResultSet rs = pstm.executeQuery();
            String temp = "";
            while(rs.next()){
                HistoryView history = new HistoryView();
//                history.setRecommendedDate((java.sql.Date)(Date) rs.getTimestamp("recommendedDate"));
//                history.setRecommendedDate(rs.getString("recommendedDate"));
                history.setRecommendedDate(rs.getString("dates"));
                history.setNama(rs.getString("nama"));
                history.setJumlah(rs.getInt("times"));
//                System.out.println(rs.getString("dates")+"--"+rs.getString("nama")+"--"+rs.getString("times"));
                histories.add(history);
            }
            
            return histories;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return histories;
    }
    
    public ArrayList<HistoryKeyView> GetKeyHistory(String dateStart, String dateEnd){
        ArrayList<HistoryKeyView> keys = new ArrayList<>();
        MyConnection mycon =  new MyConnection();
        con = mycon.getConnection();
        String query = "Select * from keywordhistory where historydate BETWEEN ? AND ? order by times desc ";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1,  dateStart);
            pstm.setString(2,  dateEnd);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                HistoryKeyView key = new HistoryKeyView();
                key.setDate(rs.getString("historydate"));
                key.setKeyword(rs.getString("keyword"));
                key.setTimes(Integer.parseInt(rs.getString("times")));
//                System.out.println(rs.getString("keyword") + "---"+ key.getTimes() );
                keys.add(key);
            }
            return keys;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
                
        return keys;
    }
    
   
    public ArrayList<Hotel> getHotelByArea(int idWilayah)
    {
        MyConnection myCon = new MyConnection();
        System.out.println("WILAYAH ADALAH"+idWilayah);
        con = myCon.getConnection();
        String query = "Select * from hotel where idWilayah = ? order by harga asc";
        ArrayList<Hotel> hotels = new ArrayList<>();
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idWilayah);
            ResultSet results = pstm.executeQuery();
            while(results.next())
            {
                
                int hotelId = results.getInt("idHotel");
                String namaHotel = results.getString("namaHotel");
                String harga = results.getString("harga");
                String contactPerson = results.getString("contactPerson");
                String jenisHotel = results.getString("jenisHotel");
                String alamatHotel = results.getString("alamat");
                String wilayah = results.getString("idWilayah");
                Hotel hotel = new Hotel(hotelId, namaHotel, alamatHotel,harga, contactPerson, jenisHotel, wilayah);
                hotels.add(hotel);
            }
            return hotels;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            myCon.logOff();
        }
        return hotels;
    }
    
    public boolean SaveTempatWisata(TourSite toursite)
    {
       MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Insert into tempatwisata values(?,?,?,?,?,?,?,?)";
       boolean result = false;
        try {
            //nama,deskripsi,htm,foto,wilayah,keyword,poin
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, 0);
            pstm.setString(2, toursite.getNama());
            pstm.setString(3, toursite.getDeskripsi());
            pstm.setInt(4, toursite.getHTM());
            pstm.setString(5, toursite.getFoto());
            pstm.setString(6, toursite.getWilayah());
            pstm.setString(7, toursite.getKeyword());
            pstm.setInt(8, toursite.getPoin());
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
                
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
               
    }
    
    public boolean DeleteTempatWisata(int tourSiteId)
    {
       MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Delete from tempatwisata where idTempatWisata = ?";
       boolean result = false;
        try {
            //nama,deskripsi,htm,foto,wilayah,keyword,poin
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, tourSiteId);
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
                
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
               
    }
    
    public boolean EditTempatWisata(TourSite toursite)
    {
//        idTempatWisata,nama,deskripsi,HTM,foto,wilayah,keyword,poin
       MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Update tempatwisata set "
               + "nama = ?, deskripsi= ?, HTM=?, foto= ?, wilayah = ?,keyword= ?,poin= ? where idTempatWisata = ?";
       boolean result = false;
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1, toursite.getNama());
            pstm.setString(2, toursite.getDeskripsi());
            pstm.setInt(3, toursite.getHTM());
            pstm.setString(4, toursite.getFoto());
            pstm.setString(5, toursite.getWilayah());
            pstm.setString(6, toursite.getKeyword());
            pstm.setInt(7, toursite.getPoin());
            pstm.setInt(8, toursite.getId());
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
                
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
               
    }
    
    public boolean SaveHotel(Hotel hotel)
    {
//        idHotel namaHotel contactPerson harga jenisHotel idWilayah alamat
        MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Insert into hotel values(?,?,?,?,?,?,?)";
       boolean result = false;
       
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, 0);
            pstm.setString(2, hotel.getNama());
            pstm.setString(3, hotel.getContactPerson());
            pstm.setString(4, hotel.getHarga());
            pstm.setString(5, hotel.getJenis());
            pstm.setString(6, hotel.getIdWilayah());
            pstm.setString(7, hotel.getAlamat());
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
                
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
       
    }
    
    public boolean DeleteHotel(int idHotel)
    {
         MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Delete from hotel where idHotel = ?";
       boolean result = false;
        try {
            //nama,deskripsi,htm,foto,wilayah,keyword,poin
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idHotel);
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
                
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
        
    }
    
    public boolean EditHotel(Hotel hotel)
    {
//        idHotel namaHotel contactPerson harga jenisHotel idWilayah alamat
       MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       String query = "Update hotel set "
               + "namaHotel = ?, contactPerson=?, harga=?, jenisHotel= ?, idWilayah = ?,alamat= ?"
               + "where idHotel = ?";
       boolean result = false;
       
        try {
            PreparedStatement pstm = con.prepareStatement(query);
          
            pstm.setString(1, hotel.getNama());
            pstm.setString(2, hotel.getContactPerson());
            pstm.setString(3, hotel.getHarga());
            pstm.setString(4, hotel.getJenis());
            pstm.setString(5, hotel.getIdWilayah());
            pstm.setString(6, hotel.getAlamat());
            pstm.setInt(7, hotel.getIdHotel());
            
            int resultSet = pstm.executeUpdate();
            if(resultSet == 1)
            {
                result = true;
            }
            
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
       
    }
    
    public ArrayList<Wilayah> GetAllWialayah(){
       MyConnection myCon = new MyConnection();
       con = myCon.getConnection();
       ArrayList<Wilayah> wilayahs = new ArrayList<>();
       String query = "Select * from wilayah";
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet results = pstm.executeQuery();
            while(results.next())
            {
             int idWilayah = results.getInt("idWilayah");
             String namaWilayah = results.getString("namaWilayah");
             Wilayah wilayah = new Wilayah(idWilayah, namaWilayah);
             wilayahs.add(wilayah);
            }
            
            return wilayahs;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return wilayahs;
    }
    
    public ArrayList<TourSite> GetAllTourSite(){
         MyConnection myCon = new MyConnection();
         con = myCon.getConnection();
         ArrayList<TourSite> tourSites = new ArrayList<>();
         String query = "Select * from tempatwisata";
         
//          pstm;
        try {
           PreparedStatement pstm = con.prepareStatement(query);
            ResultSet results = pstm.executeQuery();
            while(results.next()){
//                  String nama = results.getString("nama");
//                  String deskripsi = results.getString("nama");
                
                TourSite toursite = new TourSite();
                toursite.setNama(results.getString("nama"));
                toursite.setDeskripsi(results.getString("deskripsi"));
                toursite.setHTM(results.getInt("HTM"));
                toursite.setWilayah(results.getString("wilayah"));
                toursite.setFoto(results.getString("foto"));
                toursite.setKeyword(results.getString("keyword"));
                
                tourSites.add(toursite);
            }
            return tourSites;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            myCon.logOff();
        }
         
         return tourSites;
    }
    
    public Hotel GetHotelbyId(int idHotel){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Select * from hotel where idHotel = ?";
        Hotel hotel = new Hotel();
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idHotel);
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                hotel.setIdHotel(rs.getInt("idHotel"));
                hotel.setNama(rs.getString("namaHotel"));
                hotel.setAlamat(rs.getString("alamat"));
                hotel.setContactPerson(rs.getString("contactPerson"));
                hotel.setHarga(rs.getString("harga"));
                hotel.setIdWilayah(rs.getString("idWilayah"));
                hotel.setJenis(rs.getString("jenisHotel"));
            }
            return hotel;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
        
        return hotel;
    }
    
    public ArrayList<Hotel> GetAllHotel(){
        ArrayList<Hotel> hotels = new ArrayList<Hotel>();
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "select * from hotel";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                Hotel hotel = new Hotel();
                hotel.setIdHotel(rs.getInt("idHotel"));
                hotel.setNama(rs.getString("namaHotel"));
                hotel.setAlamat(rs.getString("alamat"));
                hotel.setContactPerson(rs.getString("contactPerson"));
                hotel.setHarga(rs.getString("harga"));
                hotel.setIdWilayah(rs.getString("idWilayah"));
                hotel.setJenis(rs.getString("jenisHotel"));
                
                hotels.add(hotel);
            }
            return hotels;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
        
        return hotels;
    }
    
    public ArrayList<Keyword> getAllKeyword(){
        ArrayList<Keyword> keywords = new ArrayList<>();
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Select * from keyword";
        
        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(query);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                Keyword keyword = new Keyword();
                keyword.setId(rs.getInt("idKeyword"));
                keyword.setParent(rs.getString("parent"));
                keyword.setKeyword(rs.getString("keyword"));
                
                keywords.add(keyword);
            }
            return keywords;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
        
        return keywords;
        
    }
    
    public Keyword getKeyword(int idKeyword){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        Keyword keyword = new Keyword();
        String query = "Select * from keyword where idKeyword = ?";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idKeyword);
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                keyword.setId(rs.getInt("idKeyword"));
                keyword.setParent(rs.getString("parent"));
                keyword.setKeyword(rs.getString("keyword"));
            }
            
            return keyword;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
        
        return keyword;
    }
    
    public void editKeyword(Keyword keyword){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "update keyword set parent = ? , keyword = ? where idKeyword = ?";
        
       
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm .setString(1, keyword.getParent());
            pstm.setString(2, keyword.getKeyword());
            pstm.setInt(3, keyword.getId());
            
            int rs = pstm.executeUpdate();
            if(rs == 1){
                System.out.println("berhasil update");
            }else{
                System.out.println("gagal update");
            }
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public boolean SaveKeyword(Keyword keyword){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Insert into keyword values(?,?,?)";
        boolean status = false;
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, 0);
            pstm.setString(2, keyword.getParent());
            pstm.setString(3, keyword.getKeyword());
            
            int rs = pstm.executeUpdate();
            if(rs == 1){
                System.out.println("berhasil insert keyword");
                status = true;
            }else{
                System.out.println("gagal insert keyword");
                
                status = false;
            }
            
            
        } catch (SQLException ex) {
            
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return status;
    }
    
    public void saveFoto(Photos photos){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "insert into foto values(?,?,?)";
        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(query);
            pstm.setInt(1, 0);
            pstm.setInt(2, photos.getIdWisata());
            pstm.setBlob(3, photos.getPhoto());
            
            int res = pstm.executeUpdate();
            if(res == 1){
                System.out.println("berhasil save foto");
            }else{
                System.out.println("gagal save foto");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
       
    }
    
    public byte[] getFoto(int id){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Select * from foto where id = ?";
        ArrayList<Photos> photos = new ArrayList<>();
//        ResultSet rs = null;
         Blob blob = null;
         byte[] imgData = null;
        try {
//           
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                blob = rs.getBlob("foto");
                imgData = blob.getBytes(1, (int) blob.length());
                return imgData;
            }else{
//                return null;
                imgData = null;
            }

            
            return imgData;
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imgData;
    }
    
    public ArrayList<Integer> getPhotoId(int idWisata){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Select * from foto where idWisata = ? and valid = 0";
        ArrayList<Integer> ids = new ArrayList<>();
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idWisata);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                ids.add(rs.getInt("id"));
            }
            return ids;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            mycon.logOff();
        }
        
        return ids;
        
    }
    
    public void updateFotoFlag(int idWisata){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "update foto set valid = 1 where idWisata = ?";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, idWisata);
            int res = pstm.executeUpdate();
            if(res == 1){
                System.out.println("berhasil set flag");
            }else{
                System.out.println("gagal set flag");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateFoto(Photos photo){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "update foto set photo = ? where id = ?";
        
        try {
            PreparedStatement pstm =  con.prepareStatement(query);
            pstm.setInt(1, photo.getId());
            pstm.setBlob(2, photo.getPhoto());
            
            int res = pstm.executeUpdate();
            if(res == 1){
                System.out.println("berhasil save foto");
            }else{
                System.out.println("gagal save foto");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void DeleteKeyword(int id){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Delete from keyword where idKeyword = ?";
        
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, id);
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int GetTotalRecomTimes(String siteName){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "Select SUM(times) AS times from recommendationhistory WHERE nama like ? ";
        int total = 0;
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setString(1,siteName);
            
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                total = rs.getInt("times");
            }else{
                total = 0;
            }
            return total;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return total;
        
    }
    
    public void setThreshold(int threshold){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        String query = "update thresholdrule set threshold = ?";
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            pstm.setInt(1, threshold);
            
            int res = pstm.executeUpdate();
            if(res == 1){
                System.out.println("berhasil set threshold");
            }else{
                System.out.println("gagal set threshold");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getThreshold(){
        MyConnection mycon = new MyConnection();
        con = mycon.getConnection();
        
        String query = "select * from thresholdrule";
        int thrs = 0;
        
        try {
            PreparedStatement pstm = con.prepareStatement(query);
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                thrs = rs.getInt("threshold");
            }else{
                thrs = 0;
            }
            
            return thrs;
        } catch (SQLException ex) {
            Logger.getLogger(TourDBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return thrs;
    }
    
    
    public static void main(String[] args) {
        TourDBO td = new TourDBO();
        ArrayList<String> keys = new ArrayList<>();
        keys.add("wisata bahari");
        keys.add("diving");
        keys.add("snorkling");
        keys.add("wisata alam");
//        keys.add("wisata belanja");
//        keys.add("wisata kuliner");
//        keys.add("halal");
//        keys.add("trade center");
        keys.add("danau");
        
//        td.GetSiteNameWAnd(keys, 2, 0);
//        td.insertKeywordHistory("wisata bahari");
//        td.updateFotoFlag(1);
//        ArrayList<Integer> ids = td.getPhotoId(1);
//        for(int id : ids){
//            System.out.println("iddd =" + id);
//        }
        
//        int t = td.GetTotalRecomTimes("Taman Laut Bunaken");
//        System.out.println("timess" + t);
        
//        td.setThreshold(60);
//        
//        System.out.println("thrsss " + td.getThreshold());
        
//        td.GetKeyHistory("2016-06-23", "2016-06-27");
//        td.GetSitesName2(keys, 1, 2);
//         TourSite toursite = new TourSite();
//            toursite.setNama("aabbb");
//            toursite.setHTM(Integer.parseInt("100"));
//            toursite.setWilayah("6");
//            toursite.setId(Integer.parseInt("9"));
//            toursite.setKeyword("diving");
//            toursite.setDeskripsi("blablablabla");
//            toursite.setFoto("0");
//            toursite.setPoin(0);
//        td.EditTempatWisata(toursite);
//           ArrayList<Hotel> hotels =  td.GetAllHotel();
//           System.out.println(hotels.size());
//        System.out.println("size" + td.GetAllTourSite().size());
//        ArrayList<Wilayah> ws = td.GetAllWialayah();
//        for ( Wilayah w : ws)
//        {
//            System.out.println(w.getNamaWilayah());
//        }
        
//        td.updateSitePoint("Pantai Malalayang");
//        TourSite a = td.GetSiteDetails("Taman Laut Bunaken");
//        System.out.println("+++++++"+a.getDeskripsi());
        
        Keyword k = new Keyword(0, "wisata bahari", "test 1");
        td.SaveKeyword(k);
        
    }
}
