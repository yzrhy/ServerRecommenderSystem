/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.dbo;

import com.me.model.People;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yizrahya
 */
public class PeopleDBO {
    
     Connection con;
     
     public People logIn(String userName, String password){
         MyConnection mycon = new MyConnection();
         con = mycon.getConnection();
         People people = new People();
         String query = "Select * from people where username = ? and password = ?";
         
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             pstm.setString(1, userName);
             pstm.setString(2, password);
             
             ResultSet rs = pstm.executeQuery();
             if(rs.next()){
                 people.setRole(rs.getString("role"));
                 people.setUsername(userName);
                 people.setPassword(password);
             }else{
                 people = null;
             }
             return people;
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         return people;
     }
     
     
     public boolean InsertPeople(People people){
         MyConnection mycon = new MyConnection();
         con = mycon.getConnection();
         
         String query = "Insert into people values (?,?,?)";
         
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             pstm.setString(1, people.getUsername());
             pstm.setString(2, people.getPassword());
             pstm.setString(3, people.getRole());
             
             int rs = pstm.executeUpdate();
             if(rs == 1){
                 System.out.println("berhasil insert");
                 return true;
             }
             else{
                 return false;
             }
                     
             
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         return true;
     }
     
     public boolean EditPeople(People people){
         MyConnection mycon = new MyConnection();
         con = mycon.getConnection();
         
         String query = "Update people set username = ?, password= ?, role=? where username = ?";
         
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             pstm.setString(1, people.getUsername());
             pstm.setString(2, people.getPassword());
             pstm.setString(3, people.getRole());
             pstm.setString(4, people.getUsername());
             
             int rs = pstm.executeUpdate();
             if(rs == 1){
                 System.out.println("berhasil update");
                 return true;
             }
             else{
                 return false;
             }
                     
             
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         return true;
     }
     
      public boolean DeletePeople(String username){
         MyConnection mycon = new MyConnection();
         con = mycon.getConnection();
         
         String query = "Delete from people where username = ?";
         
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             pstm.setString(1, username);
             
             int rs = pstm.executeUpdate();
             if(rs == 1){
                 System.out.println("berhasil update");
                 return true;
             }
             else{
                 return false;
             }
                     
             
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         return true;
     }
      
      public ArrayList<People> GetAllPeople(){
          ArrayList<People> peoples = new ArrayList<>();
          MyConnection mycon = new MyConnection();
          con = mycon.getConnection();
          
          String query = "Select * from people";
          
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             ResultSet rs = pstm.executeQuery();
             while(rs.next()){
                 People people = new People();
                 people.setUsername(rs.getString("username"));
                 people.setPassword(rs.getString("password"));
                 people.setRole(rs.getString("role"));
                 
                 peoples.add(people);
             }
             return peoples;
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
          
         return peoples;
          
      }
      
      public People GetPeople(String username){
          ArrayList<People> peoples = new ArrayList<>();
          MyConnection mycon = new MyConnection();
          con = mycon.getConnection();
          People people = new People();
          String query = "Select * from people where username = ?";
          
         try {
             PreparedStatement pstm = con.prepareStatement(query);
             pstm.setString(1, username);
             ResultSet rs = pstm.executeQuery();
             if(rs.next()){
                 people.setUsername(rs.getString("username"));
                 people.setPassword(rs.getString("password"));
                 people.setRole(rs.getString("role"));
             }
             return people;
             
         } catch (SQLException ex) {
             Logger.getLogger(PeopleDBO.class.getName()).log(Level.SEVERE, null, ex);
         }
          return people;
      }
      
      
      public static void main(String[] args) {
        PeopleDBO pdbo = new PeopleDBO();
//        People people = new People("", null, null)
        pdbo.logIn("sa", "password");
    }
}
