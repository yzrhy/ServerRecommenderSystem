/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.dbo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yizrahya
 */
public class MyConnection {
    public static final String url = "jdbc:mysql://localhost:3306/tourrecommendersys";
    public static final String DRIVER ="com.mysql.jdbc.Driver";
    public static final String USERNAME = "root";
    public static final String PASSWORD= "";
    public Connection  con = null;
    
       public Connection getConnection() 
       {
           con = null;
           try{
               Class.forName(DRIVER).newInstance();
                     con=DriverManager.getConnection(url,USERNAME,PASSWORD);
                     System.out.println("connect to database");
               }
               catch(Exception e)
               {
                   e.printStackTrace();
               }
           return con;
           }
       
    public void logOff() {
        try {
            //Tutup Koneksi
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
