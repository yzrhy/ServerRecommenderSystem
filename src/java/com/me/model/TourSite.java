/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

/**
 *
 * @author yizrahya
 */
public class TourSite {
    int id;
    String nama;
    String deskripsi;
    int HTM;
    String foto;
    String wilayah;
    String keyword;
    int poin;  
    String parents;
    int times;

    public TourSite(int id, String nama, String deskripsi, int HTM, String foto, String wilayah, String keyword, int poin, String parents, int times) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.HTM = HTM;
        this.foto = foto;
        this.wilayah = wilayah;
        this.keyword = keyword;
        this.poin = poin;
        this.parents = parents;
        this.times = times;
    }
    
    

    public TourSite(int id, String nama, String deskripsi, int HTM, String foto, String wilayah, String keyword, int poin, String parents) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.HTM = HTM;
        this.foto = foto;
        this.wilayah = wilayah;
        this.keyword = keyword;
        this.poin = poin;
        this.parents = parents;
    }
    
    

    public TourSite(int id, String nama, String deskripsi, int HTM, String foto, String wilayah, String keyword, int poin) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.HTM = HTM;
        this.foto = foto;
        this.wilayah = wilayah;
        this.keyword = keyword;
        this.poin = poin;
    }

    public TourSite() {
    }

    public String getParents() {
        return parents;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
    
    public void setParents(String parents) {
        this.parents = parents;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getHTM() {
        return HTM;
    }

    public void setHTM(int HTM) {
        this.HTM = HTM;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }
    
    
            
            
    
    
}
