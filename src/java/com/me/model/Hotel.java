/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

/**
 *
 * @author yizrahya
 */
public class Hotel {
    
    int idHotel; 
    String nama;
    String harga;
    String contactPerson;
    String jenis;
    String idWilayah;
    String alamat;

    public Hotel() {
    }

    public Hotel(int idHotel, String nama, String alamat, String harga, String contactPerson, String jenis, String idWilayah) {
        this.idHotel = idHotel;
        this.nama = nama;
        this.harga = harga;
        this.contactPerson = contactPerson;
        this.jenis = jenis;
        this.idWilayah = idWilayah;
        this.alamat = alamat;
    }


    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getIdWilayah() {
        return idWilayah;
    }

    public void setIdWilayah(String idWilayah) {
        this.idWilayah = idWilayah;
    }
    
        
}
