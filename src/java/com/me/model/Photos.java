/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

import java.io.InputStream;

/**
 *
 * @author yizrahya
 */
public class Photos {
    
    int  id;
    int idWisata;
    InputStream photo;

    public Photos() {
    }

    public Photos(int id, int idWisata, InputStream photo) {
        this.id = id;
        this.idWisata = idWisata;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdWisata() {
        return idWisata;
    }

    public void setIdWisata(int idWisata) {
        this.idWisata = idWisata;
    }

    public InputStream getPhoto() {
        return photo;
    }

    public void setPhoto(InputStream photo) {
        this.photo = photo;
    }
    
    
    
}
