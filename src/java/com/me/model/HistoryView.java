/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

import java.sql.Date;

/**
 *
 * @author yizrahya
 */
public class HistoryView {

    String nama;
    String recommendedDate;
    int jumlah;

    public HistoryView() {
    }

    
    public HistoryView(String nama, String recommendedDate, int jumlah) {
        this.nama = nama;
        this.recommendedDate = recommendedDate;
        this.jumlah = jumlah;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRecommendedDate() {
        return recommendedDate;
    }

    public void setRecommendedDate(String recommendedDate) {
        this.recommendedDate = recommendedDate;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
    
}
