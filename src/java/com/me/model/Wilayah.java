/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

/**
 *
 * @author yizrahya
 */
public class Wilayah {
    
    int idWilayah;
    String namaWilayah;

    public Wilayah(int idWilayah, String namaWilayah) {
        this.idWilayah = idWilayah;
        this.namaWilayah = namaWilayah;
    }

    public Wilayah() {
    }
    
    

    public int getIdWilayah() {
        return idWilayah;
    }

    public void setIdWilayah(int idWilayah) {
        this.idWilayah = idWilayah;
    }

    public String getNamaWilayah() {
        return namaWilayah;
    }

    public void setNamaWilayah(String namaWilayah) {
        this.namaWilayah = namaWilayah;
    }
    
    
    
}
