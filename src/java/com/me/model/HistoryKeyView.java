/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.model;

import java.sql.Date;

/**
 *
 * @author yizrahya
 */
public class HistoryKeyView {
    
    String date;
    String keyword;
    int times;

    public HistoryKeyView() {
    }

    public HistoryKeyView(String date, String keyword, int times) {
        this.date = date;
        this.keyword = keyword;
        this.times = times;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
    
    
}
