/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.helper;

import com.me.dbo.MyConnection;
import com.me.dbo.TourDBO;
import static com.me.dbo.TourDBO.PARENTS;
import com.me.model.TourSite;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author yizrahya
 */
public class TourHelper {
    
    
    public HashMap<String, Integer> cache = new HashMap<String, Integer>();
    public HashMap<String, Integer> cache2 = new HashMap<String, Integer>();
    public HashMap<String, Integer> cache3 = new HashMap<String, Integer>();
    public HashMap<String, Integer> cache4 = new HashMap<String, Integer>();
    public HashMap<String, Integer> cache5 = new HashMap<String, Integer>();
    public boolean isOtherWilayah;

    public boolean isIsOtherWilayah() {
        return isOtherWilayah;
    }

    public void setIsOtherWilayah(boolean isOtherWilayah) {
        this.isOtherWilayah = isOtherWilayah;
    }
    public static final String[] PARENTS = new String[] {"wisata bahari","wisata alam","eko wisata","wisata buatan", "wisata sejarah & budaya", "wisata religi", "wisata belanja", "wisata kuliner", "agrowisata"};
    public ArrayList<TourSite> getSites(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
//        this.setIsOtherWilayah(false);
        for(String keyword : keywords){
                System.out.println(keyword);
                sitesNames = tdbo.GetSitesName(keyword, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    System.out.println(siteName);
                    boolean isNew = cache.get(siteName) == null;
                    System.out.println("sitename: " + siteName);
//                    System.out.println("isNew :" + isNew);
                    if(isNew)
                    {
//                        System.out.println("new key:" + siteName);
                        cache.put(siteName, 1);
                    }
                    else
                    {
                        int siteValue = cache.get(siteName).intValue();
                        cache.put(siteName,siteValue+1);
//                        System.out.println("KEYY:" + siteName  +" new valuee : "+cache.get(siteName));
                    }
                }
                tdbo.insertKeywordHistory(keyword);
                
	}
//        cache.isEmpty() = true;
        if(!cache.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
//            this.setIsOtherWilayah(true);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSitesName(keyword, 0, 0);
              for(String siteName : sitesNames)
                {
                    System.out.println(siteName);
                    boolean isNew = cache.get(siteName) == null;
                    System.out.println("sitename: " + siteName);
//                    System.out.println("isNew :" + isNew);
                    if(isNew)
                    {
//                        System.out.println("new key:" + siteName);
                        cache.put(siteName, 1);
                    }
                    else
                    {
                        int siteValue = cache.get(siteName).intValue();
                        cache.put(siteName,siteValue+1);
//                        System.out.println("KEYY:" + siteName  +" new valuee : "+cache.get(siteName));
                    }
                }
             }
        }
        
        Iterator iterator = cache.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        
        if( percentage >= 60)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    }
    
    public ArrayList<TourSite> getRelatedSites(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        int threshold = tdbo.getThreshold();
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        for(String keyword : keywords){
                System.out.println(keyword);
                sitesNames = tdbo.GetSitesName(keyword, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    System.out.println(siteName);
                    boolean isNew = cache2.get(siteName) == null;
                    System.out.println("sitename: " + siteName);
//                    System.out.println("isNew :" + isNew);
                    if(isNew)
                    {
//                        System.out.println("new key:" + siteName);
                        cache2.put(siteName, 1);
                    }
                    else
                    {
                        int siteValue = cache2.get(siteName).intValue();
                        cache2.put(siteName,siteValue+1);
//                        System.out.println("KEYY:" + siteName  +" new valuee : "+cache2.get(siteName));
                    }
                }
//                tdbo.insertKeywordHistory(keyword);
                
	}
        
        if(cache2.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSitesName(keyword, 0, 0);
              for(String siteName : sitesNames)
                {
                    System.out.println(siteName);
                    boolean isNew = cache2.get(siteName) == null;
                    System.out.println("sitename: " + siteName);
//                    System.out.println("isNew :" + isNew);
                    if(isNew)
                    {
//                        System.out.println("new key:" + siteName);
                        cache2.put(siteName, 1);
                    }
                    else
                    {
                        int siteValue = cache2.get(siteName).intValue();
                        cache2.put(siteName,siteValue+1);
//                        System.out.println("KEYY:" + siteName  +" new valuee : "+cache2.get(siteName));
                    }
                }
             }
        }
        
        Iterator iterator = cache2.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache2.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache2.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache2.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        
        if( percentage < threshold)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
//                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
//       for()
       
        //insert to comment history
        
       return topSites;
    }
    
    public TourSite getSiteDetail(String siteName)
    {
        TourDBO tdbo = new TourDBO();
        TourSite tourSite = tdbo.GetSiteDetails(siteName);
        return tourSite;
        
    }
    
    public ArrayList<TourSite> getSites2(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        
                sitesNames = tdbo.GetSitesName2(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache.put(siteName, occurence);
                }
//                tdbo.insertKeywordHistory(keyword);
                
        
        if(cache.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSitesName2(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache.put(siteName, occurence);
                }
             }
        }
        
        Iterator iterator = cache.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage >= threshold)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    }
    
    public ArrayList<TourSite> getRelatedSites2(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        
//        keywords = null;
//        for(String newKey : keywords){
//             boolean isParent = Arrays.asList(PARENTS).contains(newKey);
//             
//        }
        
                sitesNames = tdbo.GetSitesName2(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache.put(siteName, occurence);
                }
//                tdbo.insertKeywordHistory(keyword);
                
        
        if(cache.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSitesName2(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
                        
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache.put(siteName, occurence);
                }
              tdbo.insertKeywordHistory(keyword);
             }
        }
        
        Iterator iterator = cache.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage < threshold )
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    }
    
    public ArrayList<TourSite> getSitesAnd(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        
                sitesNames = tdbo.GetSiteNameWAnd(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache3.put(siteName, occurence);
                }
//                tdbo.insertKeywordHistory(keyword);
                
        
        if(cache3.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSiteNameWAnd(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache3.put(siteName, occurence);
                }
             }
        }
        
        Iterator iterator = cache3.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache3.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache3.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache3.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage >= threshold)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            int times = 0;
            for(TourSite top : topSites){
                times = tdbo.GetTotalRecomTimes(top.getNama());
                top.setTimes(times);
                switch(top.getWilayah()){
                case "1": top.setWilayah("Bolaang Mongondouw"); break;
                case "2": top.setWilayah("Minahasa"); break;
                case "3": top.setWilayah("Sangihe"); break;
                case "4": top.setWilayah("Sitaro"); break;
                case "5": top.setWilayah("Talaud"); break;
                case "6": top.setWilayah("Manado"); break;
                case "7": top.setWilayah("Bitung"); break;
                case "8": top.setWilayah("Tomohon"); break;
            }
            }
            
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    }
   public ArrayList<TourSite> getSitesAnd2(ArrayList<String> keywords, int idWilayah, int budgets, int sizeB){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        this.setIsOtherWilayah(false);
        
                sitesNames = tdbo.GetSiteNameWAnd(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache5.put(siteName, occurence);
                }
                
                for(String kw : keywords){
                    tdbo.insertKeywordHistory(kw);
                }
                
                
        if(cache5.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
            this.setIsOtherWilayah(true);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSiteNameWAnd(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache5.put(siteName, occurence);
                }
             }
        }
        
        Iterator iterator = cache5.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache5.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache5.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache5.get(key);
         percentage = ((value*100)/sizeB);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage >= threshold)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            int times = 0;
            String wil = "";
            for(TourSite top : topSites){
                times = tdbo.GetTotalRecomTimes(top.getNama());
//                top.setWilayah(maxKey);
                top.setTimes(times);
                switch(top.getWilayah()){
                case "1": top.setWilayah("Bolaang Mongondouw"); break;
                case "2": top.setWilayah("Minahasa"); break;
                case "3": top.setWilayah("Sangihe"); break;
                case "4": top.setWilayah("Sitaro"); break;
                case "5": top.setWilayah("Talaud"); break;
                case "6": top.setWilayah("Manado"); break;
                case "7": top.setWilayah("Bitung"); break;
                case "8": top.setWilayah("Tomohon"); break;
            }
                
            }
            
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    } 
   public ArrayList<TourSite> getSitesAndRel2(ArrayList<String> keywords, int idWilayah, int budgets, int sizeB){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        this.setIsOtherWilayah(false);
        
                sitesNames = tdbo.GetSiteNameWAnd(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache5.put(siteName, occurence);
                }
                
                for(String kw : keywords){
                    tdbo.insertKeywordHistory(kw);
                }
                
                
        if(cache5.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
            this.setIsOtherWilayah(true);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSiteNameWAnd(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache5.put(siteName, occurence);
                }
             }
        }
        
        Iterator iterator = cache5.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache5.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache5.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache5.get(key);
         percentage = ((value*100)/sizeB);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage < threshold)
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
            int times = 0;
            String wil = "";
            for(TourSite top : topSites){
                times = tdbo.GetTotalRecomTimes(top.getNama());
//                top.setWilayah(maxKey);
                top.setTimes(times);
                switch(top.getWilayah()){
                case "1": top.setWilayah("Bolaang Mongondouw"); break;
                case "2": top.setWilayah("Minahasa"); break;
                case "3": top.setWilayah("Sangihe"); break;
                case "4": top.setWilayah("Sitaro"); break;
                case "5": top.setWilayah("Talaud"); break;
                case "6": top.setWilayah("Manado"); break;
                case "7": top.setWilayah("Bitung"); break;
                case "8": top.setWilayah("Tomohon"); break;
            }
                
            }
            
            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    } 
    public ArrayList<TourSite> getSitesAndRel(ArrayList<String> keywords, int idWilayah, int budgets){
        TourDBO tdbo = new TourDBO();
        ArrayList<String> sitesNames = new ArrayList<>();
        ArrayList<String> topSitesNames = new ArrayList<>();
        ArrayList<TourSite> topSites = new ArrayList();
        int inputCount = keywords.size();
        int match = 0;
        int percentage = 0;
        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
        String result = "";
        
                sitesNames = tdbo.GetSiteNameWOR(keywords, idWilayah, budgets);
                System.out.println("sitname" + sitesNames);
                for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache4.put(siteName, occurence);
                }
//                tdbo.insertKeywordHistory(keyword);
                
        
        if(cache4.isEmpty()){
//            sitesNames = tdbo.GetSitesName(keyword, 0, 0);
             for(String keyword : keywords){
              sitesNames = tdbo.GetSiteNameWAnd(keywords, 0, 0);
              for(String siteName : sitesNames)
                {
                    TourSite toursite2 = tdbo.GetSiteDetails(siteName);
                    List<String> itemsKey = Arrays.asList(toursite2.getKeyword().split("\\s*,\\s*"));
                    int occurence = 0;
                    for(String word : itemsKey){
//                        occurence = 0;
                        for(String wordCompare : keywords){
                            if(word.equalsIgnoreCase(wordCompare)){
                                occurence ++;
                            }
                        }
                    }
                    
                    cache4.put(siteName, occurence);
                }
             }
        }
        
        Iterator iterator = cache4.keySet().iterator();
        String maxKey  = "";
        int maxValue = 0;
        ArrayList<String> survivedKey = new ArrayList<>();
        
        while (iterator.hasNext()) {
        String key = iterator.next().toString();
        int value = cache4.get(key);
//        percentage = ((value*100)/inputCount);
            
        
        System.out.println("1111currKey:"+key +"=== currVal: "+value);
        System.out.println("valll" + value);
//            System.out.println("inputcount " + inputCount);
//            System.out.println("persentaseeee" + percentage);
        if(value > maxValue)
        {
            System.out.println("value lebih besar dari max curr");
            maxValue = value;
            maxKey = key;           
        }
     }
         boolean xx = true;
        Iterator iterator2 = cache4.keySet().iterator();
        while (iterator2.hasNext()) {
        String key = iterator2.next().toString();
        int value = cache4.get(key);
         percentage = ((value*100)/inputCount);
           System.out.println("1111currKey:"+key +"=== currVal: "+value);
            System.out.println("persentaseeee" + percentage);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
        int threshold = tdbo.getThreshold();
        if( percentage < threshold )
        {
            System.out.println("masuk");
            System.out.println("xxxx" + xx);
            if (xx)
            {
                System.out.println("update point for " + key);
                tdbo.updateSitePoint(key);
               
            
            }
            tdbo.InsertHistory(key);
            xx = false;
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
            survivedKey.add(key);
           
        }
     }
        
        for (int i = 0; i < survivedKey.size(); i++) {
            System.out.println("TOP HITS !!! "+survivedKey.get(i));
            
        }
        result = maxKey;
        System.out.println("maxk "+maxKey+" maxv "+maxValue);
        
        ArrayList<TourSite> newRes = new ArrayList<>();
       if(survivedKey.size() == 0){
            topSites = null;
       }else{
           System.out.println("masuk");
            topSites = tdbo.TopSites(survivedKey, idWilayah, budgets);
//            newRes = topSites;
//            Iterator it = topSites.iterator();
//            while(it.hasNext()){
//                for(TourSite ts : survived){
//                    topSites.removeIf(p -> p.getNama().equalsIgnoreCase(ts.getNama()));
//                }
//            }
//            for(Iterator<TourSite> tops = topSites.iterator() ; tops.hasNext();){
//                 for(TourSite ts : survived){
////                    if(tops.getNama().equalsIgnoreCase(ts.getNama())){
////                        
////                    }
//                    newRes.removeIf(p -> p.getNama().equalsIgnoreCase(ts.getNama()));
//                }
//            }
             int times = 0;
            String wil = "";
            for(TourSite top : topSites){
                times = tdbo.GetTotalRecomTimes(top.getNama());
//                top.setWilayah(maxKey);
                top.setTimes(times);
                switch(top.getWilayah()){
                case "1": top.setWilayah("Bolaang Mongondouw"); break;
                case "2": top.setWilayah("Minahasa"); break;
                case "3": top.setWilayah("Sangihe"); break;
                case "4": top.setWilayah("Sitaro"); break;
                case "5": top.setWilayah("Talaud"); break;
                case "6": top.setWilayah("Manado"); break;
                case "7": top.setWilayah("Bitung"); break;
                case "8": top.setWilayah("Tomohon"); break;
            }
                
            }
            System.out.println("tops" + topSites);
            
//            for(TourSite tops : topSites ){
//                for(TourSite ts : survived){
////                    if(tops.getNama().equalsIgnoreCase(ts.getNama())){
////                        
////                    }
//                    newRes.removeIf(p -> p.getNama().equalsIgnoreCase(ts.getNama()));
//                }
//            }
//            tdbo.insertTopRecomendations(topSites.get(0));
       }
       
        //insert to comment history
        
       return topSites;
    }
    
    public static void main(String[] args) {
        ArrayList<String> keywords1 = new ArrayList<>();
//        keywords.add("wisata bahari");
//        keywords.add("diving");
//        keywords.add("wisata alam");
//        keywords.add("danau");
//        keywords.add("restaurant");
//        keywords.add("wisata belanja");
//        keywords.add("pasar");
//        keywords.add("halal");
//        keywords.add("wisata kuliner");
//        keywords.add("restaurant");
        
        keywords1.add("wisata bahari");
        ArrayList<String> keywords = new ArrayList<>();
//        keywords.add("wisata bahari");
//        keywords.add("diving");
//        keywords.add("wisata alam");
//        keywords.add("danau");
//        keywords.add("restaurant");
//        keywords.add("wisata belanja");
//        keywords.add("pasar");
//        keywords.add("halal");
//        keywords.add("wisata kuliner");
//        keywords.add("restaurant");
        
        keywords.add("wisata bahari");
        keywords.add("diving");
        keywords.add("swimming");
//        keywords.add("jet ski");
//        keywords.add("wisata alam");
//        keywords.add("danau");
        
        TourHelper th = new TourHelper();
        System.out.println("sszz" + keywords.size());
        ArrayList<TourSite> hasil = th.getSitesAnd2(keywords, 0, 0,6);
        ArrayList<TourSite> hasilRelated = th.getSitesAndRel(keywords, 0, 0);
        if(hasilRelated != null){
            for(TourSite h :  hasilRelated)
                {
                    System.out.println("HASILLLL:" + h.getNama());
                }
        }else{
            System.out.println("tidak ada yang cocok");
        }
        
        System.out.println("IS OTHER WILAYAH" + th.isOtherWilayah);
        
//        System.out.println("OLD KEY==="+keywords);
//        keywords.removeIf(p -> p.contains("wisata belanja"));
//        System.out.println("NEW KEY==="+keywords);
        
    }
    
}
