package com.me.service;

//import ShowFoto;
import com.me.dbo.PeopleDBO;
import com.me.dbo.TourDBO;
import com.me.helper.TourHelper;
import com.me.model.HistoryKeyView;
import com.me.model.HistoryView;
import com.me.model.Hotel;
import com.me.model.Keyword;
import com.me.model.People;
import com.me.model.Photos;
import com.me.model.TourSite;
import java.sql.Blob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
//import javax.resource.cci.ResultSet;


/**
 *
 * @author yizrahya
 */
@WebService(serviceName = "RecommenderSystemServices")
public class RecommenderSystemServices {

     public HashMap<String, Integer> cache = new HashMap<String, Integer>();
     public boolean isother ;

    @WebMethod(operationName = "GetTopSitesByKeywords")
    public ArrayList<TourSite> GetTopSitesByKeywords(@WebParam(name = "keywords") ArrayList<String> keywords, @WebParam(name = "budget") int budget, @WebParam(name = "idWilayah") int idWilayah) {
        TourHelper th = new TourHelper();
        ArrayList<TourSite> hasil = th.getSites2(keywords,idWilayah, budget);  
        
        return hasil;
        
//        TourDBO tdbo = new TourDBO();
//        ArrayList<String> sitesNames = new ArrayList<>();
//        ArrayList<String> topSitesNames = new ArrayList<>();
//        ArrayList<TourSite> topSites = new ArrayList();
//        HashMap<String, Integer> sortedMap = new HashMap<String, Integer>();
//        String result = "";
//        for(String keyword : keywords){
//                System.out.println(keyword);
//                sitesNames = tdbo.GetSitesName(keyword);
//                
//                for(String siteName : sitesNames)
//                {
//                    System.out.println(siteName);
//                    boolean isNew = cache.get(siteName) == null;
//                    System.out.println("sitename: " + siteName);
//                    System.out.println("isNew :" + isNew);
//                    if(isNew)
//                    {
//                        System.out.println("new key:" + siteName);
//                        cache.put(siteName, 1);
//                    }
//                    else
//                    {
//                        int siteValue = cache.get(siteName).intValue();
//                        cache.put(siteName,siteValue+1);
//                        System.out.println("KEYY:" + siteName  +" new valuee : "+cache.get(siteName));
//                    }
//                }
//                
//	}
//        
//        Iterator iterator = cache.keySet().iterator();
//        String maxKey  = "";
//        int maxValue = 0;
//        ArrayList<String> survivedKey = new ArrayList<>();
//        
//        while (iterator.hasNext()) {
//        String key = iterator.next().toString();
//        int value = cache.get(key);
//        System.out.println("1111currKey:"+key +"=== currVal: "+value);
//        if(value > maxValue)
//        {
//            System.out.println("value lebih besar dari max curr");
//            maxValue = value;
//            maxKey = key;           
//        }
//     }
//        
//        Iterator iterator2 = cache.keySet().iterator();
//        while (iterator2.hasNext()) {
//        String key = iterator2.next().toString();
//        int value = cache.get(key);
//        System.out.println("currKey:"+key +"=== currVal: "+value);
//        if(value == maxValue)
//        {
//            System.out.println("max valueeee = "+ maxValue);
//            System.out.println("value sama besar dengan max value, add kedalam list");
//            survivedKey.add(key);
//           
//        }
//     }
//        
//        for (int i = 0; i < survivedKey.size(); i++) {
//            System.out.println("TOP HITS !!! "+survivedKey.get(i));
//            
//        }
//        result = maxKey;
//        System.out.println("maxk "+maxKey+" maxv "+maxValue);
//        
//       topSites = tdbo.TopSites(survivedKey);
//        
//       return topSites;
    }
    
 

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetSiteDetails")
    public TourSite GetSiteDetails(@WebParam(name = "siteName") String siteName) {
        //TODO write your implementation code here:
        TourDBO tdbo = new TourDBO();
        TourSite tourSite = tdbo.GetSiteDetails(siteName);
        return tourSite;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updateSitePoint")
    @Oneway
    public void updateSitePoint(@WebParam(name = "siteName") String siteName) {
        TourDBO context = new TourDBO();
        context.updateSitePoint(siteName);
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetHotelByArea")
    public ArrayList<Hotel> GetHotelByArea(@WebParam(name = "idWilayah") int idWilayah) {
        //TODO write your implementation code here:
        TourDBO context = new TourDBO();
        ArrayList<Hotel> hotels = new ArrayList<>();
        hotels= context.getHotelByArea(idWilayah);
        
        return hotels;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SaveTourSite")
    public boolean SaveTourSite(@WebParam(name = "tourSite") TourSite tourSite) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        boolean result = tdb.SaveTempatWisata(tourSite);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "EditTourSite")
    public boolean EditTourSite(@WebParam(name = "tourSite") TourSite tourSite) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        boolean result = tdb.EditTempatWisata(tourSite);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "DeleteTourSite")
    public boolean DeleteTourSite(@WebParam(name = "idTourSite") int idTourSite) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        boolean result = tdb.DeleteTempatWisata(idTourSite);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SaveHotel")
    public boolean SaveHotel(@WebParam(name = "hotel") Hotel hotel) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        boolean result = tdb.SaveHotel(hotel);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "EditHotel")
    public boolean EditHotel(@WebParam(name = "hotel") Hotel hotel) {
        TourDBO tdb = new TourDBO();
        boolean result = tdb.EditHotel(hotel);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "DeleteHotel")
    public boolean DeleteHotel(@WebParam(name = "idHotel") int idHotel) {
        TourDBO tdb = new TourDBO();
        boolean result = tdb.DeleteHotel(idHotel);
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAllTourSite")
    public ArrayList<TourSite> getAllTourSite() {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        ArrayList<TourSite> toursites = tdb.GetAllTourSite();
        return toursites;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getHotelById")
    public Hotel getHotelById(int idHotel) {
        //TODO write your implementation code here:
//        return null;
        TourDBO tdb = new TourDBO();
        return tdb.GetHotelbyId(idHotel);
    }
    
    public ArrayList<Hotel> getAllHotel(){
        TourDBO tdb = new TourDBO();
        return tdb.GetAllHotel();
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetTopRecommedations")
    public ArrayList<TourSite> GetTopRecommedations() {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.GetTopRecommendations();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetRecommnedationHistory")
    public ArrayList<HistoryView> GetRecommnedationHistory(@WebParam(name = "dateStart") String dateStart, @WebParam(name = "dateEnd") String dateEnd) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.GetHistory(dateStart, dateEnd);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "Loginuser")
    public People Loginuser(@WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        PeopleDBO pdb = new PeopleDBO();
        return pdb.logIn(username, password);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "EditPeople")
    public boolean EditPeople(@WebParam(name = "peope") People people) {
        //TODO write your implementation code here:
        PeopleDBO pdb = new PeopleDBO();
        return pdb.EditPeople(people);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "DeletePeople")
    public boolean DeletePeople(@WebParam(name = "username") String username) {
        //TODO write your implementation code here:
        PeopleDBO pdb = new PeopleDBO();
        return pdb.DeletePeople(username);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetAllPeople")
    public ArrayList<People> GetAllPeople() {
        PeopleDBO pdb = new PeopleDBO();
        return pdb.GetAllPeople();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetPeople")
    public People GetPeople(@WebParam(name = "username") String username) {
        //TODO write your implementation code here:
        PeopleDBO pdb = new PeopleDBO();
        return pdb.GetPeople(username);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetKeyHistory")
    public ArrayList<HistoryKeyView> GetKeyHistory(@WebParam(name = "dateStart") String dateStart, @WebParam(name = "dateEnd") String dateEnd) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.GetKeyHistory(dateStart, dateEnd);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SavePeople")
    public boolean SavePeople(@WebParam(name = "people") People people) {
        //TODO write your implementation code here:
        PeopleDBO pdb = new PeopleDBO();
        return pdb.InsertPeople(people);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetAllKeywords")
    public ArrayList<Keyword> GetAllKeywords() {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.getAllKeyword();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetKeyword")
    public Keyword GetKeyword(@WebParam(name = "idKeyword") int idKeyword) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.getKeyword(idKeyword);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "UpdateKeyword")
    @Oneway
    public void UpdateKeyword(@WebParam(name = "keyword") Keyword keyword) {
        TourDBO tdb = new TourDBO();
        tdb.editKeyword(keyword);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "DeleteKeyword")
    @Oneway
    public void DeleteKeyword(@WebParam(name = "idKeyword") int idKeyword) {
        TourDBO tdb = new TourDBO();
        tdb.DeleteKeyword(idKeyword);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SaveFoto")
    @Oneway
    public void SaveFoto(@WebParam(name = "photo") Photos photo) {
        TourDBO tdb = new TourDBO();
        tdb.saveFoto(photo);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetFoto")
    public byte[] GetFoto(@WebParam(name = "id") int id) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        byte[] img = tdb.getFoto(id);
        return img;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "UpdateFoto")
    @Oneway
    public void UpdateFoto(@WebParam(name = "photo") Photos photo) {
        TourDBO tdb = new TourDBO();
        try{
             tdb.updateFoto(photo);
        }catch(Exception ex){
            System.out.println("error");
        }
       
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetRelatedSitesByKeywords")
    public ArrayList<TourSite> GetRelatedSitesByKeywords(@WebParam(name = "keywords") ArrayList<String> keywords, @WebParam(name = "budget") int budget, @WebParam(name = "idWilayah") int idWilayah) {
        //TODO write your implementation code here:
         TourHelper th = new TourHelper();
        ArrayList<TourSite> hasil = th.getSitesAndRel(keywords,idWilayah, budget);  
        
        return hasil;
    }
    
    @WebMethod(operationName = "GetSitesAnd")
    public ArrayList<TourSite> GetSitesAnd(@WebParam(name = "keywords") ArrayList<String> keywords, @WebParam(name = "budget") int budget, @WebParam(name = "idWilayah") int idWilayah) {
        //TODO write your implementation code here:
         TourHelper th = new TourHelper();
        ArrayList<TourSite> hasil = th.getSitesAnd(keywords,idWilayah, budget);  
        
        return hasil;
    }
    
    @WebMethod(operationName = "GetSitesAnd2")
    public ArrayList<TourSite> GetSitesAnd2(@WebParam(name = "keywords") ArrayList<String> keywords, @WebParam(name = "budget") int budget, @WebParam(name = "idWilayah") int idWilayah, @WebParam(name = "sizeB") int sizeB) {
        //TODO write your implementation code here:
        TourHelper th = new TourHelper();
        ArrayList<TourSite> hasil = th.getSitesAnd2(keywords,idWilayah, budget, sizeB);  
        this.isother = th.isIsOtherWilayah();
        return hasil;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetTotalRecomTime")
    public int GetTotalRecomTime(@WebParam(name = "siteName") String siteName) {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.GetTotalRecomTimes(siteName);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetThreshold")
    public int GetThreshold() {
        //TODO write your implementation code here:
        TourDBO tdb = new TourDBO();
        return tdb.getThreshold();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SetThreshold")
    @Oneway
    public void SetThreshold(@WebParam(name = "threshold") int threshold) {
        TourDBO tdb = new TourDBO();
        tdb.setThreshold(threshold);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GetIsOtherWilayah")
    public boolean GetIsOtherWilayah() {
        //TODO write your implementation code here:
//        TourHelper th = new TourHelper();
        return this.isother;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SaveKeyword")
    public boolean SaveKeyword(@WebParam(name = "keyword") Keyword keyword) {
        TourDBO tdb = new TourDBO();
        return tdb.SaveKeyword(keyword);
        
    }


    
}
